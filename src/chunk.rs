use crate::value::*;

pub enum OpCode {
    Constant,
    Nil,
    True,
    False,
    Pop,
    GetLocal,
    SetLocal,
    GetGlobal,
    DefineGlobal,
    SetGlobal,
    GetUpvalue,
    SetUpvalue,
    Equal,
    Greater,
    Less,
    Add,
    Subtract,
    Multiply,
    Divide,
    Not,
    Negate,
    Print,
    Jump,
    JumpIfFalse,
    Loop,
    Call,
    Closure,
    CloseUpvalue,
    Return,
}

impl OpCode {
    pub fn from_byte(value: u8) -> OpCode {
        match value {
            x if x == OpCode::Constant as u8 => OpCode::Constant,
            x if x == OpCode::Nil as u8 => OpCode::Nil,
            x if x == OpCode::True as u8 => OpCode::True,
            x if x == OpCode::False as u8 => OpCode::False,
            x if x == OpCode::Pop as u8 => OpCode::Pop,
            x if x == OpCode::GetLocal as u8 => OpCode::GetLocal,
            x if x == OpCode::SetLocal as u8 => OpCode::SetLocal,
            x if x == OpCode::GetGlobal as u8 => OpCode::GetGlobal,
            x if x == OpCode::DefineGlobal as u8 => OpCode::DefineGlobal,
            x if x == OpCode::SetGlobal as u8 => OpCode::SetGlobal,
            x if x == OpCode::GetUpvalue as u8 => OpCode::GetUpvalue,
            x if x == OpCode::SetUpvalue as u8 => OpCode::SetUpvalue,
            x if x == OpCode::Equal as u8 => OpCode::Equal,
            x if x == OpCode::Greater as u8 => OpCode::Greater,
            x if x == OpCode::Less as u8 => OpCode::Less,
            x if x == OpCode::Add as u8 => OpCode::Add,
            x if x == OpCode::Subtract as u8 => OpCode::Subtract,
            x if x == OpCode::Multiply as u8 => OpCode::Multiply,
            x if x == OpCode::Divide as u8 => OpCode::Divide,
            x if x == OpCode::Not as u8 => OpCode::Not,
            x if x == OpCode::Negate as u8 => OpCode::Negate,
            x if x == OpCode::Print as u8 => OpCode::Print,
            x if x == OpCode::Jump as u8 => OpCode::Jump,
            x if x == OpCode::JumpIfFalse as u8 => OpCode::JumpIfFalse,
            x if x == OpCode::Loop as u8 => OpCode::Loop,
            x if x == OpCode::Call as u8 => OpCode::Call,
            x if x == OpCode::Closure as u8 => OpCode::Closure,
            x if x == OpCode::CloseUpvalue as u8 => OpCode::CloseUpvalue,
            x if x == OpCode::Return as u8 => OpCode::Return,
            _ => panic!("Invalid byte for OpCode {}", value),
        }
    }
}

pub struct Chunk {
    pub code: Vec<u8>,
    pub lines: Vec<usize>,
    pub constants: Vec<Value>,
}

impl Chunk {
    pub fn new() -> Chunk {
        Chunk {
            code: Vec::new(),
            lines: Vec::new(),
            constants: Vec::new(),
        }
    }

    pub fn write_op(&mut self, op_code: OpCode, line: usize) {
        self.write_byte(op_code as u8, line);
    }

    pub fn write_byte(&mut self, byte: u8, line: usize) {
        self.code.push(byte);
        self.lines.push(line);
    }

    pub fn add_constant(&mut self, value: Value) -> usize {
        self.constants.push(value);

        self.constants.len() - 1
    }
}