use crate::scanner::*;
use crate::chunk::OpCode;
use crate::value::Value;
use crate::object::ObjFunction;
use std::cell::RefCell;
use std::rc::Rc;
use crate::upvalue::Upvalue;
use crate::debug::*;

#[derive(Clone)]
struct Parser {
    pub current: Token,
    pub previous: Token,
    pub panic_mode: bool,
}

struct Local {
    pub name: Token,
    pub depth: i32,
    pub is_captured: bool,
}

pub struct Compiler {
    scanner: Scanner,
    parser: Parser,
    function_compilers: Vec<FunctionCompiler>,
}

pub struct FunctionCompiler {
    function: Rc<RefCell<ObjFunction>>,
    function_type: FunctionType,

    locals: Vec<Local>,
    upvalues: Vec<Upvalue>,
    scope_depth: i32,
}

impl FunctionCompiler {
    fn new(function_type: FunctionType) -> FunctionCompiler {
        FunctionCompiler {
            function: Rc::new(RefCell::new(ObjFunction::new())),
            function_type,
            locals: Vec::new(),
            upvalues: Vec::new(),
            scope_depth: 0,
        }
    }
}

#[derive(Clone, Eq, PartialEq)]
enum FunctionType {
    Function,
    Script
}

pub struct CompileError {

}

#[derive(Copy, Clone, PartialEq, PartialOrd)]
enum Precedence {
    None,
    Assignment,
    Or,
    And,
    Equality,
    Comparison,
    Term,
    Factor,
    Unary,
    Call,
    Primary,
}

impl Precedence {
    pub fn from_byte(byte: u8) -> Precedence {
        match byte {
            // Rust considered harmful?
            x if x == Precedence::None as u8 => Precedence::None,
            x if x == Precedence::Assignment as u8 => Precedence::Assignment,
            x if x == Precedence::Or as u8 => Precedence::Or,
            x if x == Precedence::And as u8 => Precedence::And,
            x if x == Precedence::Equality as u8 => Precedence::Equality,
            x if x == Precedence::Comparison as u8 => Precedence::Comparison,
            x if x == Precedence::Term as u8 => Precedence::Term,
            x if x == Precedence::Factor as u8 => Precedence::Factor,
            x if x == Precedence::Unary as u8 => Precedence::Unary,
            x if x == Precedence::Call as u8 => Precedence::Call,
            x if x == Precedence::Primary as u8 => Precedence::Primary,
            _ => panic!("Invalid precedence!"),
        }
    }

    pub fn next(&self) -> Precedence {
        let byte = *self as u8 + 1;
        Precedence::from_byte(byte)
    }
}

struct ParseRule {
    prefix: Option<Box<dyn Fn(&mut Compiler, bool) -> Result<(), CompileError>>>,
    infix: Option<Box<dyn Fn(&mut Compiler, bool) -> Result<(), CompileError>>>,
    precedence: Precedence,
}

impl Parser {
    fn new() -> Parser {
        Parser {
            current: Token {
                token_type: TokenType::EOF,
                string: String::new(),
                line: 0,
            },
            previous: Token {
                token_type: TokenType::EOF,
                string: String::new(),
                line: 0,
            },
            panic_mode: false,
        }
    }
}

impl Compiler {
    pub fn compile(source: &str) -> Result<Rc<RefCell<ObjFunction>>, CompileError> {
        let mut compiler = Compiler::new(FunctionType::Script, source);

        compiler.fun_mut().locals.reserve(u8::MAX as usize);

        compiler.advance()?;

        while !compiler.matches(TokenType::EOF)? {
            compiler.declaration()?;
        }

        Ok(compiler.end())
    }

    fn new(function_type: FunctionType, source: &str) -> Compiler {
        let mut compiler = Compiler {
            scanner: Scanner::new(source),
            parser: Parser::new(),
            function_compilers: Vec::new(),
        };

        compiler.new_function_compiler(function_type);

        compiler
    }

    fn new_function_compiler(&mut self, function_type: FunctionType) {
        let mut sub_compiler = FunctionCompiler::new(function_type.clone());

        // Setup function name for print and debugging.
        if function_type != FunctionType::Script {
            sub_compiler.function.borrow_mut().name = self.parser.previous.string.clone();
        }

        // First local is for internal use...
        sub_compiler.locals.push(Local {
            name: Token {
                token_type: TokenType::EOF,
                string: String::new(),
                line: 0,
            },
            depth: 0,
            is_captured: false,
        });

        self.function_compilers.push(sub_compiler);
    }

    fn pop_function_compiler(&mut self) {
        self.function_compilers.pop();
    }

    fn fun(&self) -> &FunctionCompiler {
        self.function_compilers.last().unwrap()
    }

    fn fun_mut(&mut self) -> &mut FunctionCompiler {
        self.function_compilers.last_mut().unwrap()
    }

    fn expression(&mut self) -> Result<(), CompileError> {
        self.parse_precedence(Precedence::Assignment)
    }

    fn block(&mut self) -> Result<(), CompileError> {
        while !self.check(TokenType::RightBrace) && !self.check(TokenType::EOF) {
            self.declaration()?;
        }

        self.consume(TokenType::RightBrace, "Expect '}' after block.")
    }

    fn func(&mut self, function_type: FunctionType) -> Result<(), CompileError> {
        self.new_function_compiler(function_type);

        let function = self.func_internal()?;
        let upvalues = self.fun().upvalues.clone();

        self.pop_function_compiler();

        let function_constant = self.make_constant(Value::Function(Rc::clone(&function)))?;
        self.emit_op_with_byte(OpCode::Closure, function_constant);

        for upvalue in upvalues {
            self.emit_byte(if upvalue.is_local { 0x1 } else { 0x0 });
            self.emit_byte(upvalue.index);
        }

        Ok(())
    }

    fn func_internal(&mut self) -> Result<Rc<RefCell<ObjFunction>>, CompileError> {
        self.begin_scope();
        self.consume(TokenType::LeftParen, "Expect '(' after function name.")?;

        if !self.check(TokenType::RightParen) {
            loop {
                self.fun().function.borrow_mut().arity += 1;
                if self.fun().function.borrow().arity > 255 {
                    return Err(self.error_at_current("Can't have more than 255 parameters."));
                }
                let constant = self.parse_variable("Expect parameter name.")?;
                self.define_variable(constant);

                if !self.matches(TokenType::Comma)? {
                    break;
                }
            }
        }

        self.consume(TokenType::RightParen, "Expect ')' after parameters.")?;
        self.consume(TokenType::LeftBrace, "Expect '{' before function body.")?;
        self.block()?;
        Ok(self.end())
    }

    fn fun_declaration(&mut self) -> Result<(), CompileError> {
        let global = self.parse_variable("Expect function name.")?;
        self.mark_initialized();
        self.func(FunctionType::Function)?;
        self.define_variable(global);
        Ok(())
    }

    fn begin_scope(&mut self) {
        self.fun_mut().scope_depth += 1;
    }

    fn end_scope(&mut self) {
        self.fun_mut().scope_depth -= 1;

        while let Some(l) = self.fun().locals.last() {
            if l.depth <= self.fun().scope_depth {
                break;
            }

            if l.is_captured {
                self.emit_op(OpCode::CloseUpvalue);
            } else {
                self.emit_op(OpCode::Pop);
            }

            self.fun_mut().locals.pop();
        }
    }

    fn var_declaration(&mut self) -> Result<(), CompileError> {
        let global = self.parse_variable("Expect variable name.")?;

        if self.matches(TokenType::Equal)? {
            self.expression()?;
        } else {
            self.emit_op(OpCode::Nil);
        }

        self.consume(TokenType::Semicolon, "Expect ';' after variable declaration.")?;

        self.define_variable(global);
        Ok(())
    }

    fn expression_statement(&mut self) -> Result<(), CompileError> {
        self.expression()?;
        self.consume(TokenType::Semicolon, "Expect ';' after expression.")?;
        self.emit_op(OpCode::Pop);
        Ok(())
    }

    fn for_statement(&mut self) -> Result<(), CompileError> {
        self.begin_scope();
        self.consume(TokenType::LeftParen, "Expect '(' after 'for'.")?;

        if self.matches(TokenType::Semicolon)? {
            // No initializer.
        } else if self.matches(TokenType::Var)? {
            self.var_declaration()?;
        } else {
            self.expression_statement()?;
        }

        let mut loop_start = self.fun().function.borrow_mut().chunk.code.len();

        let exit_jump = if !self.matches(TokenType::Semicolon)? {
            self.expression()?;
            self.consume(TokenType::Semicolon, "Expect ';' after loop condition;")?;

            Some(self.emit_jump(OpCode::JumpIfFalse))
        } else {
            None
        };

        if exit_jump.is_some() {
            self.emit_op(OpCode::Pop);
        }

        if !self.matches(TokenType::RightParen)? {
            let body_jump = self.emit_jump(OpCode::Jump);
            let increment_start = self.fun().function.borrow_mut().chunk.code.len();
            self.expression()?;
            self.consume(TokenType::RightParen, "Expect ')' after for clauses.")?;
            self.emit_op(OpCode::Pop);

            self.emit_loop(loop_start)?;
            loop_start = increment_start;
            self.patch_jump(body_jump)?;
        }

        self.statement()?;

        self.emit_loop(loop_start)?;

        // If a condition on the for loop was specified, patch the exit jump and emit the extra pop
        // for when the condition is true.
        if let Some(jump) = exit_jump {
            self.patch_jump(jump)?;
            self.emit_op(OpCode::Pop);
        }

        self.end_scope();
        Ok(())
    }

    fn if_statement(&mut self) -> Result<(), CompileError> {
        self.consume(TokenType::LeftParen, "Expect '(' after 'if'.")?;
        self.expression()?;
        self.consume(TokenType::RightParen, "Expect ')' after condition.")?;

        let then_jump = self.emit_jump(OpCode::JumpIfFalse);
        self.emit_op(OpCode::Pop);
        self.statement()?;

        let else_jump = self.emit_jump(OpCode::Jump);

        self.patch_jump(then_jump)?;
        self.emit_op(OpCode::Pop);

        if self.matches(TokenType::Else)? {
            self.statement()?;
        }

        self.patch_jump(else_jump)?;

        Ok(())
    }

    fn print_statement(&mut self) -> Result<(), CompileError> {
        self.expression()?;
        self.consume(TokenType::Semicolon, "Expect ';' after value.")?;
        self.emit_op(OpCode::Print);
        Ok(())
    }

    fn return_statement(&mut self) -> Result<(), CompileError> {
        if self.fun().function_type == FunctionType::Script {
            return Err(self.error_at_current("Can't return from top-level code."));
        }

        if self.matches(TokenType::Semicolon)? {
            self.emit_return();
        } else {
            self.expression()?;
            self.consume(TokenType::Semicolon, "Expect ';' after return value.")?;
            self.emit_op(OpCode::Return);
        }
        Ok(())
    }

    fn while_statement(&mut self) -> Result<(), CompileError> {
        let loop_start = self.fun().function.borrow_mut().chunk.code.len();
        self.consume(TokenType::LeftParen, "Expect '(' after 'while'.")?;
        self.expression()?;
        self.consume(TokenType::RightParen, "Expect ')' after condition.")?;

        let exit_jump = self.emit_jump(OpCode::JumpIfFalse);
        self.emit_op(OpCode::Pop);
        self.statement()?;

        self.emit_loop(loop_start)?;

        self.patch_jump(exit_jump)?;
        self.emit_op(OpCode::Pop);
        Ok(())
    }

    fn synchronize(&mut self) {
        self.parser.panic_mode = false;

        while self.parser.current.token_type != TokenType::EOF {
            if self.parser.previous.token_type == TokenType::Semicolon {
                return;
            }

            match self.parser.current.token_type {
                TokenType::Class => return,
                TokenType::Fun => return,
                TokenType::For => return,
                TokenType::If => return,
                TokenType::Print => return,
                TokenType::Return => return,
                TokenType::While => return,
                _ => {},
            }

            // Panic mode, discard error
            let _ = self.advance();
        }
    }

    fn declaration(&mut self) -> Result<(), CompileError> {
        if self.matches(TokenType::Fun)? {
            self.fun_declaration()?;
        } else if self.matches(TokenType::Var)? {
            self.var_declaration()?;
        } else {
            self.statement()?;
        }

        if self.parser.panic_mode {
            self.synchronize();
        }

        return Ok(());
    }

    fn statement(&mut self) -> Result<(), CompileError> {
        if self.matches(TokenType::Print)? {
            return self.print_statement();
        } else if self.matches(TokenType::For)? {
            return self.for_statement();
        } else if self.matches(TokenType::If)? {
            return self.if_statement();
        } else if self.matches(TokenType::Return)? {
            return self.return_statement();
        } else if self.matches(TokenType::While)? {
            return self.while_statement();
        } else if self.matches(TokenType::LeftBrace)? {
            self.begin_scope();
            self.block()?;
            self.end_scope();
            Ok(())
        }
        else {
            return self.expression_statement();
        }
    }

    fn grouping(&mut self, _can_assign: bool) -> Result<(), CompileError> {
        self.expression()?;
        self.consume(TokenType::RightParen, "Expect ')' after expression.")?;
        Ok(())
    }

    fn number(&mut self, _can_assign: bool) -> Result<(), CompileError> {
        let value = self.parser.previous.string.parse::<f64>().unwrap();
        self.emit_constant(Value::Number(value))
    }

    fn or(&mut self, _can_assign: bool) -> Result<(), CompileError> {
        let else_jump = self.emit_jump(OpCode::JumpIfFalse);
        let end_jump = self.emit_jump(OpCode::Jump);
        self.patch_jump(else_jump)?;
        self.emit_op(OpCode::Pop);
        self.parse_precedence(Precedence::Or)?;
        self.patch_jump(end_jump)?;
        Ok(())
    }

    fn string(&mut self, _can_assign: bool) -> Result<(), CompileError> {
        let mut string = self.parser.previous.string.clone();
        string.remove(0);
        string.pop();
        self.emit_constant(Value::String(string))
    }

    fn named_variable(&mut self, name: Token, can_assign: bool) -> Result<(), CompileError> {
        let mut arg = self.resolve_local(name.clone(), self.function_compilers.len() - 1)?;

        let (get_op, set_op) = if arg.is_some() {
            (OpCode::GetLocal, OpCode::SetLocal)
        } else {
            arg = self.resolve_upvalue(&name)?;
            if arg.is_some() {
                (OpCode::GetUpvalue, OpCode::SetUpvalue)
            } else {
                // TODO int types here feel inconsistent.
                arg = Some(self.identifier_constant(name)?);
                (OpCode::GetGlobal, OpCode::SetGlobal)
            }
        };

        if can_assign && self.matches(TokenType::Equal)? {
            self.expression()?;
            self.emit_op_with_byte(set_op, arg.unwrap());
        } else {
            self.emit_op_with_byte(get_op, arg.unwrap());
        }

        Ok(())
    }

    fn variable(&mut self, can_assign: bool) -> Result<(), CompileError> {
        self.named_variable(self.parser.previous.clone(), can_assign)
    }

    fn unary(&mut self, _can_assign: bool) -> Result<(), CompileError> {
        let operator_type = self.parser.previous.token_type;

        self.parse_precedence(Precedence::Unary)?;

        match operator_type {
            TokenType::Minus => self.emit_op(OpCode::Negate),
            TokenType::Bang => self.emit_op(OpCode::Not),
            _ => (),
        }
        Ok(())
    }

    fn binary(&mut self, _can_assign: bool) -> Result<(), CompileError> {
        let operator_type = self.parser.previous.token_type;
        let rule = Compiler::get_rule(operator_type);
        self.parse_precedence(rule.precedence.next())?;

        match operator_type {
            TokenType::Plus => self.emit_op(OpCode::Add),
            TokenType::Minus => self.emit_op(OpCode::Subtract),
            TokenType::Star => self.emit_op(OpCode::Multiply),
            TokenType::Slash => self.emit_op(OpCode::Divide),
            TokenType::BangEqual => self.emit_ops(OpCode::Equal, OpCode::Not),
            TokenType::EqualEqual => self.emit_op(OpCode::Equal),
            TokenType::Greater => self.emit_op(OpCode::Greater),
            TokenType::GreaterEqual => self.emit_ops(OpCode::Less, OpCode::Not),
            TokenType::Less => self.emit_op(OpCode::Less),
            TokenType::LessEqual => self.emit_ops(OpCode::Greater, OpCode::Not),
            _ => panic!("Unexpected TokenType!"),
        }
        Ok(())
    }

    fn call(&mut self, _can_assign: bool) -> Result<(), CompileError> {
        let arg_count = self.argument_list()?;
        self.emit_op_with_byte(OpCode::Call, arg_count);
        Ok(())
    }

    fn literal(&mut self, _can_assign: bool) -> Result<(), CompileError> {
        match self.parser.previous.clone().token_type {
            TokenType::False => self.emit_op(OpCode::False),
            TokenType::Nil => self.emit_op(OpCode::Nil),
            TokenType::True => self.emit_op(OpCode::True),
            _ => unreachable!(),
        }
        Ok(())
    }

    fn parse_precedence(&mut self, precedence: Precedence) -> Result<(), CompileError> {
        self.advance()?;
        let prefix_rule = Compiler::get_rule(self.parser.previous.token_type).prefix;
        let can_assign = precedence <= Precedence::Assignment;

        match prefix_rule {
            Some(fun) => fun(self, can_assign)?,
            None => return Err(self.error_at_current("Expect expression.")),   // TODO Should it be a panic? Should the error already have been caught in Scanner?
        }

        while precedence <= Compiler::get_rule(self.parser.current.token_type).precedence {
            self.advance()?;
            let infix_rule = Compiler::get_rule(self.parser.previous.token_type).infix.unwrap();
            infix_rule(self, can_assign)?;
        }

        if can_assign && self.matches(TokenType::Equal)? {
            return Err(self.error_at_current("Invalid assignment target."));
        }

        Ok(())
    }

    fn identifier_constant(&mut self, name: Token) -> Result<u8, CompileError> {
        self.make_constant(Value::String(name.string.clone()))
    }

    fn add_local(&mut self, name: Token) -> Result<(), CompileError> {
        if self.fun().locals.len() == u8::MAX as usize {
            return Err(self.error_at_current("Too many local variables in function."));
        }
        self.fun_mut().locals.push(Local {
            name,
            depth: -1,  // -1 means uninitialized.
            is_captured: false,
        });

        Ok(())
    }

    fn identifiers_equal(a: &Token, b: &Token) -> bool {
        a.string == b.string
    }

    fn resolve_local(&mut self, name: Token, function_compiler_idx: usize) -> Result<Option<u8>, CompileError> {
        for (idx, local) in self.function_compilers[function_compiler_idx].locals.iter().enumerate().rev() {
            if Compiler::identifiers_equal(&name, &local.name) {
                if local.depth == - 1 {
                    return Err(self.error_at_current("Can't read local variable in its own initializer."));
                }
                return Ok(Some(idx as u8));
            }
        }
        Ok(None)
    }

    fn resolve_upvalue(&mut self, name: &Token) -> Result<Option<u8>, CompileError> {
        let mut upvalue = None;
        let mut is_local = true;

        for idx in (0..(self.function_compilers.len()) - 1).rev() {
            if let Some(local) = self.resolve_local(name.clone(), idx)? {
                upvalue = Some(local);
                break;
            }
            is_local = false;
        }

        if let Some(local) = upvalue {
            let outer_function_compiler_idx = self.function_compilers.len() - 2;
            self.function_compilers[outer_function_compiler_idx].locals[local as usize].is_captured = true;
            return Ok(Some(self.add_upvalue(local, is_local)));
        }

        Ok(None)
    }

    fn add_upvalue(&mut self, index: u8, is_local: bool) -> u8 {
        for (idx, upvalue) in self.fun().upvalues.iter().enumerate() {
            if upvalue.index == index && upvalue.is_local == is_local {
                return idx as u8;
            }
        }

        self.fun_mut().upvalues.push(Upvalue {
            is_local,
            index,
        });

        self.fun().function.borrow_mut().upvalue_count += 1;
        assert!(self.fun().upvalues.len() == self.fun().function.borrow().upvalue_count);

        (self.fun().upvalues.len() - 1) as u8
    }

    fn declare_variable(&mut self) -> Result<(), CompileError> {
        if self.fun().scope_depth == 0 {
             return Ok(());
        }

        let name = self.parser.previous.clone();

        for local in self.fun().locals.iter().rev() {
            if local.depth != -1 && local.depth < self.fun().scope_depth {
                break;
            }

            if Compiler::identifiers_equal(&name, &local.name) {
                return Err(self.error_at_current("Already a variable with this name in this scope."));
            }
        }

        self.add_local(name)
    }

    fn parse_variable(&mut self, error_message: &str) -> Result<u8,CompileError> {
        self.consume(TokenType::Identifier, error_message)?;

        self.declare_variable()?;
        if self.fun().scope_depth > 0 {
            // TODO It shouldn't be an error?
            return Ok(0)
        }

        self.identifier_constant(self.parser.previous.clone())
    }

    fn mark_initialized(&mut self) {
        if self.fun().scope_depth == 0 {
             return;
        }
        self.fun_mut().locals.last_mut().unwrap().depth = self.fun().scope_depth;
    }

    fn define_variable(&mut self, global: u8) {
        if self.fun().scope_depth > 0 {
            self.mark_initialized();
            return;
        }
        self.emit_op_with_byte(OpCode::DefineGlobal, global);
    }

    fn argument_list(&mut self) -> Result<u8, CompileError> {
        let mut arg_count = 0;
        if !self.check(TokenType::RightParen) {
            loop {
                self.expression()?;
                if arg_count == 255 {
                    return Err(self.error_at_current("Can't have more than 255 arguments."));
                }
                arg_count += 1;
                if !self.matches(TokenType::Comma)? {
                    break;
                }
            }
        }
        self.consume(TokenType::RightParen, "Expect ')' after arguments.")?;
        Ok(arg_count)
    }

    fn and(&mut self, _can_assign: bool) -> Result<(), CompileError> {
        let end_jump = self.emit_jump(OpCode::JumpIfFalse);

        self.emit_op(OpCode::Pop);

        self.parse_precedence(Precedence::And)?;
        self.patch_jump(end_jump)?;

        Ok(())
    }

    fn advance(&mut self) -> Result<(), CompileError> {
        self.parser.previous = self.parser.current.clone();
        let mut had_error = false;

        loop {
            let result = self.scan_token();

            match result {
                Ok(token) => {
                    self.parser.current = token;
                    break;
                },
                Err(e) => {
                    self.error_at_current(&*e.message);
                    had_error = true;
                }
            }
        }

        // TODO Not consistent with book, we also need to silence error messages in this case. Having a
        // synchronize function like jlox may be better.
        if had_error {
            Err(CompileError{})
        } else {
            Ok(())
        }
    }

    fn scan_token(&mut self) -> Result<Token, ScanError> {
        self.scanner.scan_token()
    }

    fn consume(&mut self, token_type: TokenType, message: &str) -> Result<(), CompileError> {
        if self.parser.current.token_type == token_type {
            self.advance()
        } else {
            Err(self.error_at_current(message))
        }
    }

    fn check(&self, token_type: TokenType) -> bool {
        self.parser.current.token_type == token_type
    }

    fn matches(&mut self, token_type: TokenType) -> Result<bool, CompileError> {
        if !self.check(token_type) {
            return Ok(false);
        }

        self.advance()?;
        Ok(true)
    }

    fn error_at_current(&mut self, message: &str) -> CompileError {
        self.error_at(self.parser.current.clone(), message)
    }

    fn error_at(&mut self, token: Token, message: &str) -> CompileError {
        eprint!("[line {}] Error", token.line);

        match token.token_type {
            TokenType::EOF => {
                eprint!(" at end");
            },
            _ => {
                eprint!(" at '{}'", token.string);
            }
        }
        eprint!(": {}\n", message);

        self.parser.panic_mode = true;
        CompileError{}
    }

    fn get_rule(token_type: TokenType) -> ParseRule {
        match token_type {
            TokenType::LeftParen => ParseRule       { prefix: Some(Box::new(Compiler::grouping)), infix: Some(Box::new(Compiler::call)), precedence: Precedence::Call, },
            TokenType::RightParen => ParseRule      { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::LeftBrace => ParseRule       { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::RightBrace => ParseRule      { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::Comma => ParseRule           { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::Dot => ParseRule             { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::Minus => ParseRule           { prefix: Some(Box::new(Compiler::unary)), infix: Some(Box::new(Compiler::binary)), precedence: Precedence::Term, },
            TokenType::Plus => ParseRule            { prefix: None, infix: Some(Box::new(Compiler::binary)), precedence: Precedence::Term, },
            TokenType::Semicolon => ParseRule       { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::Slash => ParseRule           { prefix: None, infix: Some(Box::new(Compiler::binary)), precedence: Precedence::Factor, },
            TokenType::Star => ParseRule            { prefix: None, infix: Some(Box::new(Compiler::binary)), precedence: Precedence::Factor, },
            TokenType::Bang => ParseRule            { prefix: Some(Box::new(Compiler::unary)), infix: None, precedence: Precedence::None, },
            TokenType::BangEqual => ParseRule       { prefix: None, infix: Some(Box::new(Compiler::binary)), precedence: Precedence::Equality, },
            TokenType::Equal => ParseRule           { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::EqualEqual => ParseRule      { prefix: None, infix: Some(Box::new(Compiler::binary)), precedence: Precedence::Equality, },
            TokenType::Greater => ParseRule         { prefix: None, infix: Some(Box::new(Compiler::binary)), precedence: Precedence::Comparison, },
            TokenType::GreaterEqual => ParseRule    { prefix: None, infix: Some(Box::new(Compiler::binary)), precedence: Precedence::Comparison, },
            TokenType::Less => ParseRule            { prefix: None, infix: Some(Box::new(Compiler::binary)), precedence: Precedence::Comparison, },
            TokenType::LessEqual => ParseRule       { prefix: None, infix: Some(Box::new(Compiler::binary)), precedence: Precedence::Comparison, },
            TokenType::Identifier => ParseRule      { prefix: Some(Box::new(Compiler::variable)), infix: None, precedence: Precedence::None, },
            TokenType::String => ParseRule          { prefix: Some(Box::new(Compiler::string)), infix: None, precedence: Precedence::None, },
            TokenType::Number => ParseRule          { prefix: Some(Box::new(Compiler::number)), infix: None, precedence: Precedence::None, },
            TokenType::And => ParseRule             { prefix: None, infix: Some(Box::new(Compiler::and)), precedence: Precedence::And, },
            TokenType::Break => ParseRule           { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::Class => ParseRule           { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::Else => ParseRule            { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::False => ParseRule           { prefix: Some(Box::new(Compiler::literal)), infix: None, precedence: Precedence::None, },
            TokenType::Fun => ParseRule             { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::For => ParseRule             { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::If => ParseRule              { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::Nil => ParseRule             { prefix: Some(Box::new(Compiler::literal)), infix: None, precedence: Precedence::None, },
            TokenType::Or => ParseRule              { prefix: None, infix: Some(Box::new(Compiler::or)), precedence: Precedence::Or, },
            TokenType::Print => ParseRule           { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::Return => ParseRule          { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::Super => ParseRule           { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::This => ParseRule            { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::True => ParseRule            { prefix: Some(Box::new(Compiler::literal)), infix: None, precedence: Precedence::None, },
            TokenType::Var => ParseRule             { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::While => ParseRule           { prefix: None, infix: None, precedence: Precedence::None, },
            TokenType::EOF => ParseRule             { prefix: None, infix: None, precedence: Precedence::None, },
        }
    }

    fn emit_op(&mut self, op: OpCode) {
        let line = self.parser.previous.line;
        self.fun().function.borrow_mut().chunk.write_op(op, line);
    }

    fn emit_ops(&mut self, op1: OpCode, op2: OpCode) {
        let line = self.parser.previous.line;
        self.fun().function.borrow_mut().chunk.write_op(op1, line);
        self.fun().function.borrow_mut().chunk.write_op(op2, line);
    }

    fn emit_op_with_byte(&mut self, op: OpCode, byte: u8) {
        let line = self.parser.previous.line;
        self.fun().function.borrow_mut().chunk.write_op(op, line);
        self.fun().function.borrow_mut().chunk.write_byte(byte, line);
    }

    fn emit_byte(&mut self, byte: u8) {
        let line = self.parser.previous.line;
        self.fun().function.borrow_mut().chunk.write_byte(byte, line);
    }

    fn emit_loop(&mut self, loop_start: usize) -> Result<(), CompileError> {
        self.emit_op(OpCode::Loop);

        let offset = self.fun().function.borrow_mut().chunk.code.len() - loop_start + 2;
        if offset > u16::MAX as usize {
            return Err(self.error_at_current("Loop body too large."));
        }

        self.emit_byte((offset >> 8) as u8 & 0xff);
        self.emit_byte(offset as u8 & 0xff);
        Ok(())
    }

    fn emit_jump(&mut self, instruction: OpCode) -> usize {
        self.emit_op(instruction);
        self.emit_byte(0xff);
        self.emit_byte(0xff);
        self.fun().function.borrow_mut().chunk.code.len() - 2
    }

    fn emit_return(&mut self) {
        self.emit_op(OpCode::Nil);
        self.emit_op(OpCode::Return);
    }

    fn emit_constant(&mut self, value: Value) -> Result<(), CompileError> {
        let constant_idx = self.make_constant(value)?;
        self.emit_op_with_byte(OpCode::Constant, constant_idx);
        Ok(())
    }

    fn patch_jump(&mut self, offset: usize) -> Result<(), CompileError> {
        let jump = self.fun().function.borrow_mut().chunk.code.len() - offset - 2;

        if jump > u16::MAX as usize {
            return Err(self.error_at_current("Too much code to jump over."));
        }

        let jump0 = ((jump >> 8) & 0xff) as u8;
        let jump1 = (jump & 0xff) as u8;

        self.fun().function.borrow_mut().chunk.code[offset] = jump0;
        self.fun().function.borrow_mut().chunk.code[offset + 1] = jump1;
        Ok(())
    }

    fn make_constant(&mut self, value: Value) -> Result<u8, CompileError> {
        let constant = self.fun().function.borrow_mut().chunk.add_constant(value);

        if constant > u8::MAX as usize {
            return Err(self.error_at_current("Too many constants in one chunk."));
        }

        Ok(constant as u8)
    }

    fn end(&mut self) -> Rc<RefCell<ObjFunction>>{
        self.emit_return();

        #[cfg(feature = "lox_debug_print_code")]
        {
            let name = if self.fun().function.borrow().name.is_empty() { String::from("<script>") } else { self.fun().function.borrow().name.clone() };

            disassemble_chunk(&self.fun().function.borrow().chunk, name.as_str());
        }

        Rc::clone(&self.fun().function)
    }
}