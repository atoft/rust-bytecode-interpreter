use crate::chunk::*;
use crate::value::Value;

pub fn disassemble_chunk(chunk: &Chunk, name: &str) {
    println!("== {} ==", name);

    let mut idx = 0;
    while idx < chunk.code.len() {
        idx = disassemble_instruction(chunk, idx);
    }
}

pub fn disassemble_instruction(chunk: &Chunk, offset: usize) -> usize {
    print!("{:04} ", offset);

    if offset > 0 && chunk.lines[offset] == chunk.lines[offset - 1] {
        print!("   | ");
    } else {
        print!("{:4} ", chunk.lines[offset]);
    }

    let instruction = chunk.code[offset];
    match OpCode::from_byte(instruction) {
        OpCode::Constant => constant_instruction("OP_CONSTANT", chunk, offset),
        OpCode::Nil => simple_instruction("OP_NIL", offset),
        OpCode::True => simple_instruction("OP_TRUE", offset),
        OpCode::False => simple_instruction("OP_FALSE", offset),
        OpCode::Pop => simple_instruction("OP_POP", offset),
        OpCode::GetLocal => byte_instruction("OP_GET_LOCAL", chunk, offset),
        OpCode::SetLocal => byte_instruction("OP_SET_LOCAL", chunk, offset),
        OpCode::GetGlobal => constant_instruction("OP_GET_GLOBAL", chunk, offset),
        OpCode::DefineGlobal => constant_instruction("OP_DEFINE_GLOBAL", chunk, offset),
        OpCode::SetGlobal => constant_instruction("OP_SET_GLOBAL", chunk, offset),
        OpCode::GetUpvalue => byte_instruction("OP_GET_UPVALUE", chunk, offset),
        OpCode::SetUpvalue => byte_instruction("OP_SET_UPVALUE", chunk, offset),
        OpCode::Equal => simple_instruction("OP_EQUAL", offset),
        OpCode::Greater => simple_instruction("OP_GREATER", offset),
        OpCode::Less => simple_instruction("OP_LESS", offset),
        OpCode::Add => simple_instruction("OP_ADD", offset),
        OpCode::Subtract => simple_instruction("OP_SUBTRACT", offset),
        OpCode::Multiply => simple_instruction("OP_MULTIPLY", offset),
        OpCode::Divide => simple_instruction("OP_DIVIDE", offset),
        OpCode::Not => simple_instruction("OP_NOT", offset),
        OpCode::Negate => simple_instruction("OP_NEGATE", offset),
        OpCode::Print => simple_instruction("OP_PRINT", offset),
        OpCode::Jump => jump_instruction("OP_JUMP", 1, chunk, offset),
        OpCode::JumpIfFalse => jump_instruction("OP_JUMP_IF_FALSE", 1, chunk, offset),
        OpCode::Loop => jump_instruction("OP_LOOP", -1, chunk, offset),
        OpCode::Call => byte_instruction("OP_CALL", chunk, offset),
        OpCode::Closure => {
            let constant = chunk.code[offset + 1];
            print!("{:<16} {:4} ", "OP_CLOSURE", constant);
            print_value(&chunk.constants[constant as usize]);
            print!("\n");

            let mut extra_offset = 0;

            if let Value::Function(function) = &chunk.constants[constant as usize] {
                for idx in 0..(function.borrow().upvalue_count) {
                    let is_local = chunk.code[offset + 2 + idx * 2];
                    let index = chunk.code[offset + 2 + idx * 2 + 1];
                    print!("{:04}       |     {} {} \n", offset + 2 + idx * 2, if is_local != 0 { "local"} else { "upvalue"}, index);
                }
                extra_offset = function.borrow().upvalue_count * 2 + 2;
            }

            offset + 2 + extra_offset
        },
        OpCode::CloseUpvalue => simple_instruction("OP_CLOSE_UPVALUE", offset),
        OpCode::Return => simple_instruction("OP_RETURN", offset),
    }
}

pub fn print_value(value: &Value) {
    match value {
        Value::Number(n) => {
            print!("{:.4}", n);
        },
        Value::Bool(b) => {
            print!("{}", b);
        },
        Value::String(s) => {
            print!("{}", s)
        },
        Value::Function(f) => {
            f.borrow().print()
        },
        Value::Closure(c) => {
            c.borrow().function.borrow().print()
        }
        Value::Native(_) => {
            print!("<native fn>")
        }
        Value::Nil => {
            print!("nil")
        },
    }
}

fn simple_instruction(name: &str, offset: usize) -> usize {
    println!("{:<16}", name);
    offset + 1
}

fn byte_instruction(name: &str, chunk: &Chunk, offset: usize) -> usize {
    let slot = chunk.code[offset + 1];
    println!("{:<16} {:4}", name, slot);
    offset + 2
}

fn jump_instruction(name: &str, sign: i32, chunk: &Chunk, offset: usize) -> usize {
    let jump = (chunk.code[offset + 1] as u16) << 8 | (chunk.code[offset + 2] as u16);
    println!("{:<16} {:4} -> {}\n", name, offset, (offset as i32) + 3 + sign * (jump as i32));
    offset + 3
}

fn constant_instruction(name: &str, chunk: &Chunk, offset: usize) -> usize {
    let constant_idx = chunk.code[offset + 1];
    print!("{:<16} {:4} '", name, constant_idx);
    print_value(&chunk.constants[constant_idx as usize]);
    print!("'\n");
    offset + 2
}

