use crate::vm::Vm;

pub mod compiler;
pub mod chunk;
pub mod debug;
pub mod scanner;
pub mod value;
pub mod vm;
pub mod object;
pub mod upvalue;

#[derive(Debug)]
pub enum RunFileError {
    IOError,
    LexerError,
    ParseError,
    ResolutionError,
    RuntimeError,
}
pub fn run_file(path: String) -> Result<(), RunFileError> {
    println!("Passed path {}", path);
    let contents = match std::fs::read_to_string(path) {
        Ok(c) => c,
        Err(_) => return Err(RunFileError::IOError),
    };

    Vm::interpret(&contents).or(Err(RunFileError::RuntimeError))
}
