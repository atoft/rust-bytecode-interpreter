use rust_bytecode_interpreter::*;
use std::process;

fn main() {
    let args = std::env::args();
    if args.len() == 2 {
        match run_file(args.last().unwrap()) {
            Ok(()) => {
                process::exit(0)
            }
            Err(e) => match e {
                RunFileError::IOError | RunFileError::LexerError | RunFileError::ParseError | RunFileError::ResolutionError => {
                    process::exit(65);
                }
                RunFileError::RuntimeError => {
                    process::exit(70);
                }
            }
        }
    } else if args.len() == 1 {
        println!("REPL not implemented.");
    } else {
        println!("Usage: rust-interpreter [script]");
        process::exit(64);
    }
}
