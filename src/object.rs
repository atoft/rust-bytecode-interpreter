use crate::chunk::*;
use crate::value::Value;
use crate::vm::InterpretError;
use std::rc::Rc;
use std::cell::RefCell;
use crate::vm::Vm;

pub struct ObjFunction {
    pub arity: usize,
    pub upvalue_count: usize,
    pub chunk: Chunk,
    pub name: String,
}

pub type ObjNative = Rc<dyn Fn(&mut Vm, u8) -> Result<Value, InterpretError>>;

pub struct ObjClosure {
    pub function: Rc<RefCell<ObjFunction>>,
    pub upvalues: Vec<Rc<RefCell<ObjUpvalue>>>,
}

impl ObjClosure {
    pub fn new(function: Rc<RefCell<ObjFunction>>) -> ObjClosure {
        ObjClosure {
            function,
            upvalues: Vec::new(),
        }
    }
}

impl ObjFunction {
    pub fn new() -> ObjFunction {
        ObjFunction {
            arity: 0,
            upvalue_count: 0,
            chunk: Chunk::new(),
            name: String::new(),
        }
    }

    pub fn print(&self) {
        print!("<fn {}>", self.name)
    }
}

pub struct ObjUpvalue {
    pub location: usize, // This is Value* in clox. It points directly into the stack while the upvalue is open. It points to `closed` directly when the upvalue is closed.
    pub closed: Option<Value>,
}

impl ObjUpvalue {
    pub fn new(location: usize) -> ObjUpvalue {
        ObjUpvalue {
            location,
            closed: None,
        }
    }
}
