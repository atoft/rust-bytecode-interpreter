
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum TokenType {
    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    Comma,
    Dot,
    Minus,
    Plus,
    Semicolon,
    Slash,
    Star,
    Bang,
    BangEqual,
    Equal,
    EqualEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual,
    Identifier,
    String,
    Number,
    And,
    Break,
    Class,
    Else,
    False,
    Fun,
    For,
    If,
    Nil,
    Or,
    Print,
    Return,
    Super,
    This,
    True,
    Var,
    While,
    EOF,
}

#[derive(Clone)]
pub struct Token {
    pub token_type: TokenType,
    pub string: String, // @Lang: Would be better as a slice of the input.
    pub line: usize,
}

#[derive(Debug)]
pub struct ScanError {
    pub message: String,
    pub line: usize,
}

#[derive(Clone)]
pub struct Scanner {
    pub chars: Vec<char>,
    pub start: usize,
    pub current: usize,
    pub line: usize,
}

impl Scanner {
    pub fn new(source: &str) -> Scanner {
        let chars: Vec<char> = source.chars().collect();

        Scanner {
            chars,
            start: 0,
            current: 0,
            line: 1,
        }
    }

    pub fn scan_token(&mut self) -> Result<Token, ScanError> {
        self.skip_whitespace();
        self.start = self.current;

        if self.is_at_end() {
            return Ok(self.make_token(TokenType::EOF));
        }

        let character = self.advance();

        let token_type = match character {
            '(' => self.make_token(TokenType::LeftParen),
            ')' => self.make_token(TokenType::RightParen),
            '{' => self.make_token(TokenType::LeftBrace),
            '}' => self.make_token(TokenType::RightBrace),
            ',' => self.make_token(TokenType::Comma),
            '.' => self.make_token(TokenType::Dot),
            '-' => self.make_token(TokenType::Minus),
            '+' => self.make_token(TokenType::Plus),
            ';' => self.make_token(TokenType::Semicolon),
            '*' => self.make_token(TokenType::Star),
            '/' => self.make_token(TokenType::Slash),
            '!' => {
                if self.advance_if('=') {
                    self.make_token(TokenType::BangEqual)
                } else {
                    self.make_token(TokenType::Bang)
                }
            }
            '=' => {
                if self.advance_if('=') {
                    self.make_token(TokenType::EqualEqual)
                } else {
                    self.make_token(TokenType::Equal)
                }
            }
            '<' => {
                if self.advance_if('=') {
                    self.make_token(TokenType::LessEqual)
                } else {
                    self.make_token(TokenType::Less)
                }
            }
            '>' => {
                if self.advance_if('=') {
                    self.make_token(TokenType::GreaterEqual)
                } else {
                    self.make_token(TokenType::Greater)
                }
            }
            '"' => {
                self.scan_string()?
            }
            digit if Scanner::is_digit(digit) => {
                self.scan_number()
            }
            alpha if Scanner::is_alphabetic(alpha) => {
                self.scan_identifier()
            }
            _ => {
                return Err(self.make_error(
                    format!("Unexpected character '{}'", character)
                ));
            }
        };

        Ok(token_type)
    }

    fn skip_whitespace(&mut self) {
        loop {
            let character = self.peek();
            match character {
                ' ' | '\r' | '\t' => {
                    self.advance();
                },
                '\n' => {
                    self.line += 1;
                    self.advance();
                },
                '/' => {
                    if self.peek_next() == '/' {
                        while self.peek() != '\n' && !self.is_at_end() {
                            self.advance();
                        }
                    } else {
                        return;
                    }
                },
                _ => {
                    return;
                }
            }
        }
    }

    fn is_at_end(&self) -> bool {
        self.current > self.chars.len() - 1
    }

    fn make_token(&self, token_type: TokenType) -> Token {
        Token {
            token_type,
            string: self.chars[self.start..self.current].into_iter().collect(),
            line: self.line,
        }
    }

    fn make_error(&self, message: String) -> ScanError {
        ScanError {
            message,
            line: self.line,
        }
    }

    fn scan_string(&mut self) -> Result<Token, ScanError> {
        while self.peek() != '"' && !self.is_at_end() {
            if self.peek() == '\n' {
                self.line += 1;
            }
            self.advance();
        }
        if self.is_at_end() {
            return Err(self.make_error(String::from("Unterminated string.")));
        }
        // Skip the closing quote.
        self.advance();

        Ok(self.make_token(TokenType::String))
    }

    fn scan_number(&mut self) -> Token {
        while Scanner::is_digit(self.peek()) {
            self.advance();
        }
        if self.peek() == '.' && Scanner::is_digit(self.peek_next()) {
            self.advance();
            while Scanner::is_digit(self.peek()) {
                self.advance();
            }
        }

        self.make_token(TokenType::Number)
    }

    fn scan_identifier(&mut self) -> Token {
        while self.peek().is_alphanumeric() {
            self.advance();
        }

        self.make_token(self.identifier_type())
    }

    fn identifier_type(&self) -> TokenType {
        match self.chars[self.start] {
            // Source code is stored as Vec<char>, so string literals need to be converted to match.
            // Can Rust optimize this? Probably not?
            'a' => self.check_keyword(1, &"nd".chars().collect(), TokenType::And),
            'c' => self.check_keyword(1, &"lass".chars().collect(), TokenType::Class),
            'e' => self.check_keyword(1, &"lse".chars().collect(), TokenType::Else),
            'f' => {
                if self.current - self.start > 1 {
                    match self.chars[self.start + 1] {
                        'a' => self.check_keyword(2, &"lse".chars().collect(), TokenType::False),
                        'o' => self.check_keyword(2, &"r".chars().collect(), TokenType::For),
                        'u' => self.check_keyword(2, &"n".chars().collect(), TokenType::Fun),
                        _ => TokenType::Identifier,
                    }
                } else {
                    TokenType::Identifier
                }
            }
            'i' => self.check_keyword(1, &"f".chars().collect(), TokenType::If),
            'n' => self.check_keyword(1, &"il".chars().collect(), TokenType::Nil),
            'o' => self.check_keyword(1, &"r".chars().collect(), TokenType::Or),
            'p' => self.check_keyword(1, &"rint".chars().collect(), TokenType::Print),
            'r' => self.check_keyword(1, &"eturn".chars().collect(), TokenType::Return),
            's' => self.check_keyword(1, &"uper".chars().collect(), TokenType::Super),
            't' => {
                if self.current - self.start > 1 {
                    match self.chars[self.start + 1] {
                        'h' => self.check_keyword(2, &"is".chars().collect(), TokenType::This),
                        'r' => self.check_keyword(2, &"ue".chars().collect(), TokenType::True),
                        _ => TokenType::Identifier,
                    }
                } else {
                    TokenType::Identifier
                }
            }
            'v' => self.check_keyword(1, &"ar".chars().collect(), TokenType::Var),
            'w' => self.check_keyword(1, &"hile".chars().collect(), TokenType::While),
            _ => TokenType::Identifier,
        }
    }

    fn check_keyword(&self, start: usize, rest: &Vec<char>, token_type: TokenType) -> TokenType {
        if self.current - self.start == start + rest.len() {
            if self.chars[self.start + start..self.start + start + rest.len()] == rest[..] {
                return token_type;
            }
        }
        TokenType::Identifier
    }

    fn is_digit(character: char) -> bool {
        character >= '0' && character <= '9'
    }

    fn is_alphabetic(character: char) -> bool {
        character.is_alphabetic()
    }

    fn advance(&mut self) -> char {
        let result = self.chars[self.current];
        self.current += 1;
        result
    }

    fn advance_if(&mut self, query: char) -> bool {
        if self.is_at_end() {
            return false;
        }
        if self.chars[self.current] != query {
            return false;
        }
        self.current += 1;
        true
    }

    fn peek(&self) -> char {
        if self.is_at_end() {
            return '\0';
        }
        return self.chars[self.current];
    }

    fn peek_next(&self) -> char {
        if self.current + 1 >= self.chars.len() {
            return '\0';
        }
        return self.chars[self.current + 1];
    }

}