
#[derive(Copy, Clone)]
pub struct Upvalue {
    pub index: u8,
    pub is_local: bool,
}