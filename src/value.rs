use std::cell::RefCell;
use std::rc::Rc;


use crate::object::*;

pub type FunctionRef = Rc<RefCell<ObjFunction>>;

#[derive(Clone)]
pub enum Value {
    Number(f64),
    Bool(bool),
    String(String),
    Function(FunctionRef),
    Closure(Rc<RefCell<ObjClosure>>),
    Native(ObjNative),
    Nil,
}