use crate::chunk::OpCode;
use crate::compiler::*;
use crate::debug::*;
use crate::value::*;
use std::collections::HashMap;
use std::rc::Rc;
use std::cell::RefCell;
use crate::object::ObjNative;
use std::time::SystemTime;
use crate::object::ObjClosure;
use crate::object::ObjUpvalue;

pub struct CallFrame
{
    pub closure: Rc<RefCell<ObjClosure>>,
    pub ip: usize,
    pub slot_idx: usize,
}

pub struct Vm {
    pub frames: Vec<CallFrame>,

    // @Lang: I'd like an InPlaceVec<Value, 256>, i.e. a locally-allocated Vec that can store up to
    // 256 elements.
    pub stack: Vec<Value>,

    pub globals: HashMap<String, Value>,

    pub open_upvalues: Vec<Rc<RefCell<ObjUpvalue>>>,
}

pub enum InterpretError {
    CompileError,
    RuntimeError,
}

impl Vm {
    pub fn interpret(source: &str) -> Result<(), InterpretError> {
        let function = if let Ok(fun) = Compiler::compile(source) {
            fun
        } else {
            return Err(InterpretError::CompileError); // TODO
        };

        let mut vm = Vm {
            frames: Vec::new(),
            stack: Vec::new(),
            globals: HashMap::new(),
            open_upvalues: Vec::new(),
        };

        vm.define_native("clock", Rc::new(Vm::clock_native));
        vm.define_native("panic", Rc::new(Vm::panic_native));

        let closure = Rc::new(RefCell::new(ObjClosure::new(function)));
        vm.push(Value::Closure(Rc::clone(&closure)));
        vm.call(closure, 0)?;

        vm.run()
    }

    fn run(&mut self) -> Result<(), InterpretError> {
        loop {
            #[cfg(feature = "lox_debug_trace_execution")]
            {
                print!("        ");
                for value in &self.stack {
                    print!("[ ");
                    print_value(value);
                    print!(" ]");
                }
                println!();

                let ip = self.frame().ip;
                disassemble_instruction(&self.frame().closure.borrow().function.borrow().chunk, ip);
            }

            let instruction = OpCode::from_byte(self.read_byte());
            match instruction {
                OpCode::Constant => {
                    let constant = self.read_constant();
                    self.push(constant);
                },
                OpCode::Nil => {
                    self.push(Value::Nil);
                },
                OpCode::True => {
                    self.push(Value::Bool(true));
                },
                OpCode::False => {
                    self.push(Value::Bool(false));
                },
                OpCode::Pop => {
                    self.pop();
                }
                OpCode::GetLocal => {
                    let slot = self.read_byte();
                    let slot_offset = self.frame().slot_idx;
                    self.push(self.stack[slot as usize + slot_offset].clone());
                }
                OpCode::SetLocal => {
                    let slot = self.read_byte();
                    let slot_offset = self.frame().slot_idx;
                    self.stack[slot as usize + slot_offset] = self.peek();
                }
                OpCode::GetGlobal => {
                    let name = self.read_string();
                    if let Some(v) = self.globals.get(&name) {
                        let value = v.clone();
                        self.push(value);
                    } else {
                        let err = format!("Undefined variable '{}'.", name);
                        return Err(self.runtime_error(err.as_str()));
                    };
                }
                OpCode::GetUpvalue => {
                    let slot = self.read_byte();
                    let rc_closure = Rc::clone(&self.frame().closure);
                    let closure = rc_closure.borrow();
                    let upvalue = closure.upvalues[slot as usize].borrow();

                    // Branching is needed compared to clox because we have no way to point upvalue.location to upvalue.closed.
                    if let Some(value) = &upvalue.closed {
                        self.push(value.clone());
                    } else {
                        let value_idx = upvalue.location;
                        self.push(self.stack[value_idx].clone());
                    }
                },
                OpCode::SetUpvalue => {
                    let slot = self.read_byte();
                    let rc_closure = Rc::clone(&self.frame().closure);
                    let closure = rc_closure.borrow();
                    let mut upvalue = closure.upvalues[slot as usize].borrow_mut();

                    // Branching is needed compared to clox because we have no way to point upvalue.location to upvalue.closed.
                    if let Some(value) = &mut upvalue.closed {
                        *value = self.peek();
                    } else {
                        let value_idx = upvalue.location;
                        self.stack[value_idx] = self.peek();
                    }
                },
                OpCode::DefineGlobal => {
                    let name = self.read_string();
                    self.globals.insert(name, self.peek());
                    let _ = self.pop();
                }
                OpCode::SetGlobal => {
                    let name = self.read_string();
                    if let None = self.globals.insert(name.clone(), self.peek()) {
                        let _ = self.globals.remove(&name);
                        let err = format!("Undefined variable '{}'.", name);
                        return Err(self.runtime_error(err.as_str()));
                    }
                    // Setting a global doesn't pop like defining does.
                }
                OpCode::Equal => {
                    let b = self.pop();
                    let a = self.pop();
                    self.push(Value::Bool(Vm::values_equal(&a, &b)));
                },
                OpCode::Greater => {
                    let (a, b) = self.pop_two_numbers()?;
                    self.push(Value::Bool(a > b));
                },
                OpCode::Less => {
                    let (a, b) = self.pop_two_numbers()?;
                    self.push(Value::Bool(a < b));
                }
                OpCode::Add => {
                    if let Some((a, b)) = self.try_pop_two_numbers() {
                        self.push(Value::Number(a + b));
                    } else if let Some((a, b)) = self.try_pop_two_strings() {
                        self.push(Value::String(a + b.as_str()));
                    } else {
                        return Err(self.runtime_error("Operands must be two numbers or two strings"));
                    }
                },
                OpCode::Subtract => {
                    let (a, b) = self.pop_two_numbers()?;
                    self.push(Value::Number(a - b));
                },
                OpCode::Multiply => {
                    let (a, b) = self.pop_two_numbers()?;
                    self.push(Value::Number(a * b));
                },
                OpCode::Divide => {
                    let (a, b) = self.pop_two_numbers()?;
                    self.push(Value::Number(a / b));
                },
                OpCode::Not => {
                    let b = self.pop();
                    self.push(Value::Bool(Vm::is_falsey(&b)));
                }
                OpCode::Negate => {
                    if let Value::Number(n) = self.peek() {
                        self.pop();
                        self.push(Value::Number(-n));
                    } else {
                        panic!("Tried to negate not number");
                    }
                },
                OpCode::Print => {
                    print_value(&self.pop());
                    println!();
                }
                OpCode::Jump => {
                    let offset = self.read_short();
                    self.frame().ip += offset as usize;
                }
                OpCode::JumpIfFalse => {
                    let offset = self.read_short();
                    if Vm::is_falsey(&self.peek()) {
                        self.frame().ip += offset as usize;
                    }
                }
                OpCode::Loop => {
                    let offset = self.read_short();
                    self.frame().ip -= offset as usize;
                }
                OpCode::Call => {
                    let arg_count = self.read_byte();
                    let function_value = self.peek_at(arg_count as usize);
                    self.call_value(function_value, arg_count as usize)?;
                }
                OpCode::Closure => {
                    let function = if let Value::Function(f) = self.read_constant() {
                        f
                    } else {
                        panic!("Tried to define closure from non-function value.");
                    };
                    let closure = Rc::new(RefCell::new(ObjClosure::new(Rc::clone(&function))));
                    self.push(Value::Closure(Rc::clone(&closure)));
                    for _upvalue_idx in 0..function.borrow().upvalue_count {
                        let is_local = self.read_byte();
                        let index = self.read_byte();
                        if is_local != 0 {
                            let frame_slot_idx = self.frame().slot_idx;
                            closure.borrow_mut().upvalues.push(self.capture_upvalue(frame_slot_idx + index as usize));
                        } else {
                            closure.borrow_mut().upvalues.push(Rc::clone(&self.frame().closure.borrow().upvalues[index as usize]));
                        }
                    }
                }
                OpCode::CloseUpvalue => {
                    self.close_upvalues(self.stack.len() - 1);
                    self.pop();
                },
                OpCode::Return => {
                    let prev_slots = self.frames.last().unwrap().slot_idx;
                    self.close_upvalues(prev_slots);
                    let result = self.pop();
                    self.frames.pop();
                    if self.frames.is_empty() {
                        self.pop();
                        return Ok(())
                    }

                    self.stack.truncate(prev_slots);
                    self.push(result);
                },
            }
        }
    }

    fn pop_two_numbers(&mut self) -> Result<(f64, f64), InterpretError> {
        if let Some(pair) = self.try_pop_two_numbers() {
            Ok(pair)
        } else {
            Err(self.runtime_error("Operands must be two numbers"))
        }
    }

    fn try_pop_two_numbers(&mut self) -> Option<(f64, f64)> {
        if let Value::Number(_) = self.peek_at(0) {
            if let Value::Number(_) = self.peek_at(1) {
                let b = self.pop();
                let a = self.pop();

                if let Value::Number(a_n) = a {
                    if let Value::Number(b_n) = b {
                        return Some((a_n, b_n));
                    } else {
                        unreachable!();
                    }
                } else {
                    unreachable!();
                }
            }
        }
        return None;
    }

    fn try_pop_two_strings(&mut self) -> Option<(String, String)> {
        if let Value::String(_) = self.peek_at(0) {
            if let Value::String(_) = self.peek_at(1) {
                let b = self.pop();
                let a = self.pop();

                if let Value::String(a_s) = a {
                    if let Value::String(b_s) = b {
                        return Some((a_s, b_s));
                    } else {
                        unreachable!();
                    }
                } else {
                    unreachable!();
                }
            }
        }
        return None;
    }

    fn frame(&mut self) -> &mut CallFrame {
        self.frames.last_mut().unwrap()
    }

    fn byte_at(&self, ip: usize) -> u8 {
        self.frames.last().unwrap().closure.borrow().function.borrow().chunk.code[ip]
    }

    fn read_byte(&mut self) -> u8 {
        let ip = self.frame().ip;
        let val = self.byte_at(ip);
        self.frame().ip += 1;
        val
    }

    fn read_string(&mut self) -> String {
        let name = self.read_constant();
        let name_string = if let Value::String(s) = name { s } else { panic!() };
        name_string
    }

    fn read_constant(&mut self) -> Value {
        let constant_idx = self.read_byte() as usize;
        self.frame().closure.borrow().function.borrow_mut().chunk.constants[constant_idx].clone()
    }

    fn read_short(&mut self) -> u16 {
        let ip = self.frame().ip;
        let val0 = self.byte_at(ip) as u16;
        let val1 = self.byte_at(ip + 1) as u16;
        self.frame().ip += 2;
        (val0 << 8) | val1
    }

    fn push(&mut self, value: Value) {
        self.stack.push(value);
    }

    fn pop(&mut self) -> Value {
        self.stack.pop().expect("Tried to pop empty stack!")
    }

    fn peek(&self) -> Value {
        self.stack.last().expect("Tried to peek empty stack!").clone()
    }

    fn peek_at(&self, distance: usize) -> Value {
        self.stack[self.stack.len() - 1 - distance].clone()
    }

    fn call_value(&mut self, callee: Value, arg_count: usize) -> Result<(), InterpretError> {
        match callee {
            Value::Closure(c) => {
                self.call(c, arg_count)
            }
            Value::Native(n) => {
                let stack_last = self.stack.len() - 1;
                let return_value = n(self, arg_count as u8)?;
                self.stack.truncate(stack_last - arg_count);
                self.stack.push(return_value);
                Ok(())
            }
            _ => {
                Err(self.runtime_error("Can only call functions and classes."))
            }
        }
    }

    fn capture_upvalue(&mut self, local: usize) -> Rc<RefCell<ObjUpvalue>> {
        let mut upvalue = None;
        let mut found_idx = 0;

        for (idx, existing_upvalue) in self.open_upvalues.iter().enumerate() {
            let upvalue_location = existing_upvalue.borrow().location;
            if upvalue_location > local {
                found_idx = idx;
                break;
            } else if upvalue_location == local {
                upvalue = Some(Rc::clone(existing_upvalue));
                break;
            }
        }

        if let Some(upvalue) = upvalue {
            return upvalue;
        }

        let created_upvalue = Rc::new(RefCell::new(ObjUpvalue::new(local)));

        self.open_upvalues.insert(found_idx, Rc::clone(&created_upvalue));

        created_upvalue
    }

    fn close_upvalues(&mut self, last_idx: usize) {
        let mut new_open_upvalue_idx = None;

        for (idx, open_upvalue) in self.open_upvalues.iter().enumerate() {
            // TODO Do we ever call this and close no upvalues?
            if open_upvalue.borrow().location < last_idx {
                break;
            }

            let value = self.stack[open_upvalue.borrow().location].clone();
            open_upvalue.borrow_mut().closed = Some(value);
            new_open_upvalue_idx = Some(idx + 1);
        }

        if let Some(idx) = new_open_upvalue_idx {
            self.open_upvalues.drain(0..idx);
        }
    }

    fn call(&mut self, callee: Rc<RefCell<ObjClosure>>, arg_count: usize) -> Result<(), InterpretError> {
        let arity = callee.borrow().function.borrow().arity;
        if arg_count != arity {
            let msg = format!("Expected {} arguments but got {}.", arity, arg_count);
            return Err(self.runtime_error(&msg));
        }

        const FRAMES_MAX: usize = 255;
        if self.frames.len() == FRAMES_MAX {
            return Err(self.runtime_error("Stack overflow."));
        }

        let frame = CallFrame {
            closure: Rc::clone(&callee),
            ip: 0,
            slot_idx: self.stack.len() - arg_count - 1,
        };
        self.frames.push(frame);
        Ok(())
    }

    fn is_falsey(value: &Value) -> bool {
        match value {
            Value::Number(_) => false,
            Value::Bool(b) => !b,
            Value::String(_) => false,
            Value::Closure(_) => false,
            Value::Function(_) => false,
            Value::Native(_) => false,
            Value::Nil => true,
        }
    }

    fn values_equal(lhs: &Value, rhs: &Value) -> bool {
        match (lhs, rhs) {
            (Value::Bool(l), Value::Bool(r)) => l == r,
            (Value::Nil, Value::Nil) => true,
            (Value::Number(l), Value::Number(r)) => l == r,
            (Value::String(l), Value::String(r)) => l == r,
            (Value::Function(l), Value::Function(r)) => FunctionRef::ptr_eq(l, r),
            (_,_) => false,
        }
    }

    fn runtime_error(&mut self, message: &str) -> InterpretError {
        eprintln!("{}", message);

        for frame in self.frames.iter().rev() {
            let function_name = if frame.closure.borrow().function.borrow().name.is_empty() {
                String::from("script")
            } else {
                format!("{}()", frame.closure.borrow().function.borrow().name)
            };
            eprintln!("[line {}] in {}", frame.closure.borrow().function.borrow().chunk.lines[frame.ip], function_name);
        }

        self.reset_stack();
        InterpretError::RuntimeError
    }

    fn define_native(&mut self, name: &str, function: ObjNative) {
        // clox puts these on the stack for GC reasons.
        //self.push(Value::String(String::from(name)));
        //self.push(Value::Native(function));
        self.globals.insert(String::from(name), Value::Native(function));
        //self.pop()
        //self.pop()
    }

    fn clock_native(&mut self, _arg_count: u8) -> Result<Value, InterpretError> {
        match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
            Ok(n) => Ok(Value::Number(n.as_secs_f64())),
            Err(_) => panic!("SystemTime before UNIX EPOCH!"),
        }
    }

    fn panic_native(&mut self, arg_count: u8) -> Result<Value, InterpretError> {
        if arg_count == 1 {
            if let Value::String(s) = self.peek() {
                return Err(self.runtime_error(&format!("Script panicked: {}", s)));
            }
        }
        Err(self.runtime_error("Script panicked."))
    }

    fn reset_stack(&mut self) {
        self.stack.clear();
    }
}